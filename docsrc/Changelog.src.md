# ChangeLog

## Other Notes
- 2023-11-28: Move dirs: .docsrc, .config, code => docsrc, config, src
- 2023-11-28: Add `$cli->exclude_from_help[] = 'main';` to hide any command from the help menu

## Git Log
`git log` on @system(date, trim)`
@system(git log --pretty=format:'- %h %d %s [%cr]' --abbrev-commit)
