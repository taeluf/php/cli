# Php Cli Framework
A (hopefully) simple framework for running cli apps. It is not very feature rich. (no automatic help menus and not really any built-in cli functions)

## Install
@template(php/composer_install, taeluf/cli)

## Get Started
Use the built-in cli to generate a cli starter script for you, then edit it to fit your needs.

```bash
vendor/bin/tlf-cli setup
```

## Usage
Generally, you'll make an executable file with the `#!/usr/bin/env php` shebang at the top, then a `<?php` on the next line, then your script to follow.

Here is a sample we use for testing
```php
@file(test/proto)
```

## Testing this lib
I don't use my testing framework, since my testing framework uses this & a circular dependency like that is just a headache.

To test this lib during development run `php test/test.php`

The tests are simple & should stay that way.

## TODO
- Write better documentation???
