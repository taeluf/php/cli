<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# ChangeLog  
  
## Other Notes  
- 2023-11-28: Move dirs: .docsrc, .config, code => docsrc, config, src
- 2023-11-28: Add `$cli->exclude_from_help[] = 'main';` to hide any command from the help menu  
  
## Git Log  
`git log` on Tue Nov 28 09:34:58 PM CST 2023`  
- bc7a5c2  (HEAD -> v0.1) update composer [5 seconds ago]  
- 6595946  (origin/v0.1) add exclude_from_help property to cli class [2 minutes ago]  
- 7933a8a  bugfix minor error with print_table [29 hours ago]  
- ad897a9  add print_table() method, to print similar to how mysql cli does [30 hours ago]  
- 8fee575  add $main_on_command_not_found = false property to allow main to be run when there are arguments. [2 weeks ago]  
- fb127a0  run scrawl [9 weeks ago]  
- c3b4071  hopefully fixed autloader inclusion [9 weeks ago]  
- b962ecf  add tlf-clifor easier setup [9 weeks ago]  
- 3df78ea  add prompt() for string prompts. Added docblocks. Slightly improved ask question formatting [9 weeks ago]  
- 2a39939  add notice method [7 months ago]  
- 0a66b9d  run scrawl [7 months ago]  
- fa09a14  trim each line of a logged message [8 months ago]  
- 73c0682  trim message, then append newline to end [8 months ago]  
- 621931a  add logging method [8 months ago]  
- ab3067f  composer update [12 months ago]  
- 8293092  define argv for php 8.2 compatability [12 months ago]  
- f90037f  add ask() method to cli [1 year, 6 months ago]  
- 9ee3ed8  add help menu [1 year, 6 months ago]  
- 1b5f917  add error() method & msgs trait [1 year, 6 months ago]  
- 04bf8fe  update scrawl, run scrawl [1 year, 7 months ago]  
- eb3ae3d  documentation [1 year, 8 months ago]  
- e08da2c  docs, and add load_json_file() method [1 year, 8 months ago]  
- cd42303  add call_command() method [1 year, 8 months ago]  
- ad54fab  fix: add classmap autoloader to composer.json [2 years ago]  
- afb1b80  notes [2 years ago]  
- 0c3de98  minor changes & I think it works [2 years ago]  
- 7c8b1e8  notes [2 years ago]  
- 734d843  tests passing. add notes [2 years ago]  
- 0307358  init repo [2 years ago]  
