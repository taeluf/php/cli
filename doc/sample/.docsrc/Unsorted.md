<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Unsorted Docs  
Just a melting pot for all my documentation  
For other Documentation, see:  
- [Extensions](doc/Extensions.md)  
- [Regex Matching in Files](doc/RegexMatching.md)  
- [Other Technical Stuff](doc/TechnicalStuff.md)  
  
  
## MD Verb Extensions  
Md verb extensions are called when an `<!-- MdVerb `@verb()` has no handler -->` is found in a markdown source file.  
  
### @classMethods  
- `<!-- MdVerb `@classMethods()` has no handler -->`  
    - `@arg ClassName`: The class name. You may specify namespace as well.  
    - `@arg visibility`: `*` for *any visibility*, or `public`, or `private`, or `protected`  
Displays all methods of a class with the given visibility.   
  
#### @classMethods example  
`@‌classMethods(Tlf\Scrawl\Utility\Php, public)` displays:  
<!-- MdVerb `@classMethods()` has no handler -->  
  
  
  
## Executing Code Scrawl  
You can execute `scrawl`. You can interact with the `Tlf\Cli` class directly in php, or with the `Tlf\Scrawl` class directly.  
  
For now, you can look at the code base. Also, try running `scrawl` & saying `no`(well, `n`, technically) to the prompt  
  
  
## Configs for Code Scrawl  
  
### To know:  
- Configs go in `PROJECT_ROOT/.scrawl.json` or `PROJECT_ROOT/.config/scrawl.json`  
- In cli `scrawl --boolAttribute` or `scrawl -strAttribute strValue` to override your `json` configurations.   
    - `--boolAttribute` presence means "set `boolAttribute` to true"  
- Your `.json` file is loaded by `Tlf\Cli` (when you use the `scrawl` command). If you instantiate `Tlf\Scrawl($projectDir, $configs)` in PHP, you'd have to load up configs yourself.  
- `--deleteExistingDocs` is a DANGEROUS feature. If `true` in your `.json` or provided in your `scrawl` call, your entire `docs` folder will be deleted when running `scrawl`.  
- Configs in the PHP array provided to `Tlf\Scrawl` do NOT use `-` or `--`. This is purely for predictable cli input parsing.  
- `--markdown.preserveNewLines` Adds 2 spaces at the end of every line to ensure Markdown parsers separate those lines in output.   
    - Defaults `true`... thus MUST be disabled through `.json`  
- There's also a `-scrawl.ext` config option to load additional extensions. See the config file for this package for an example.   
    - @TODO properly document `-scrawl.ext` config.  
  
### Available configs & their default values:  
All default values are determined/picked in the `Tlf\Cli` class  
# Import key "Configs.list" not found.  
