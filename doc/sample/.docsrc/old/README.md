<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Code Scrawl  
A documentation generation framework with some built-in extensions for exporting comments and code, and importing into markdown.  
  
## Status  
Under development. The documentation is in bad shape. Today is April 1, 2021. Hoping this will be in great shape by the end of April.  
  
## Getting Started  
### 1. Install  
1. Download: `git clone https://gitlab.com/taeluf/php/CodeScrawl.git CodeScrawl` (put this anywhere you like)  
2. Modify bashrc: `echo "export PATH=\""$(pwd)/CodeScrawl/cli:\$PATH"\"" >> ~/.bashrc;`  
    - To manually edit: `gedit ~/.bashrc`, or `xed ~/.bashrc`, `vim ~/.bashrc`, `nano ~/.bashrc`  
3. Reload terminal: `source ~/.bashrc`  
  
### 2. Simple Example  
1. Create some folders & files:  
    - `docs/` <- Output directory  
    - `docs-src/` <- Documenation template files  
    - `docs-src/Test.src.md` <- A template file  
    - `src/SomeFile.php` <- It doesn't have to be PHP.  
2. Add a docblock export to your code:  
    - ```php  
      /**   
      * I am a docblock  
      * <!-- MdVerb `@export()` has no handler -->  
      */  
      ```  
      @TODO support alternative docblock syntax, such as `##\n#\n#`  
3. Add a wrapped export to your code:  
    - ```php  
      // <!-- MdVerb `@export_start()` has no handler -->  
      echo "I am an echo";  
      // <!-- MdVerb `@export_end()` has no handler -->  
      ```  
      Also supports `# @export_...`  
4. Import into your docs. In `docs-src/Test.src.md`, write:  
    - ```md  
      # Example of Docblock Import:  
      # Import key "Docblock.Test" not found.  
  
      # Example of Wrapped Import:  
      This is a nice way to auto copy+paste production or test code into your markdown file.  
      `‌`‌`php  
      # Import key "WrappedCode.Test" not found.  
      `‌`‌`  
      ```  
5. Run Code Scrawl. `cd` into your project directory, then execute `scrawl`.  
6. Review `docs/Test.md`. The `# Import key "KEY" not found.` calls will be replaced  
  
### 3. Doing More  
Now you have the basics. Please submit an issue to improve our docs if this intro has been insufficient.  
  
- Configuration: Stored in `YOUR_PROJECT/.config/scrawl.json` or `YOUR_PROJECT/.scrawl.json`. See [docs/Config.md](docs/Config.md)  
- Regex Matching: See [doc/RegexMatching.md](doc/RegexMatching.md)  
- Extensions: See [doc/Extensions.md](docs/Extensions.md). You can make shared extensions for others to use or personal extensions that do something very specific for your project.  
- Cli Options & Direct PHP execution: See [doc/CliAndPhp.md](doc/CliAndPhp.md)  
- Additional technical Stuff: See [doc/TechnicalStuff.md](doc/TechnicalStuff.md)  
- TODOs: See [doc/Todo.md](doc/Todo.md)  
