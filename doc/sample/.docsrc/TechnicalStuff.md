<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Technical Stuff  
Things you probably don't need to know unless you have a problem.  
  
## Have a question?  
If you're not happy with the docs, you can browse the source code or submit an issue. Better yet, do both! & send a PR :D <3  
  
## Writing a literal @import inside markdown  
If you don't want an import to be done. You just want to write `@import`  
You'll need a zero-width-non-joiner between the `@` sign and `import`. You can copy+paste this: `@‍import(ExampleOfImportKey)` or [do an internet](https://www.ecosia.org/search?q=zero-width+non-joiner)  
  
## deleteExistingDocs  
Existing docs can only be deleted if they pass the sanity checks, as follows:  
```php  
<?php  
# Import key "Technical.deleteExistingDocs.sanityCheck" not found.  
```  
  
## Default Configs  
Default Configs are as follows:  
# Import key "Configs.list" not found.  
  
The actual code for deriving these defaults is:  
```php  
<?php  
# Import key "Configs.defaultsCode" not found.  
```  
