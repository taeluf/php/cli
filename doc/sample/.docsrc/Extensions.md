<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Extensions for Code Scrawl  
  
## Working with Regex  
If your extension deals with any regex, you should use the [Regex Matching Utility](RegexMatching.md) to make things way cleaner & easier  
  
## Extensions Api  
### The Gist:  
- To expose something to `# Import key "CustomKey" not found.`, call `addOutput('key', 'CustomKey', 'The string to import')`  
- To create a file, call `addOutput('file', 'rel/path.ext', 'This is the file content')`  
- To write an extension, extend `\Tlf\Scrawl\Extension`, add it to the `scrawl.ext` config (see example), & override one or more of the extension Methods (bottom of page)  
  
### Extensions  
For working with the set of available extensions (and adding to it)  
- `addExtension($group, $extension)`: Add an extension to a group.  
    - if `$group=='default'`, `$extension` should `extends \Tlf\Scrawl\Extension` or `implements \Tlf\Scrawl\ExtensionInterface`  
    - if `$group=='docblock'`, `$extension` should be a `$callable` accepting `(\Tlf\Scrawl\DocBlock $docBlock)`  
    - else, it depends completely upon the code that calls the extension.  
- `getExtensions($group)`: Get an array of extensions for the given group  
- `extCall($group, $methodName, ...$arg)`: Loops over `$getExtensions($group)` & calls `$ext->$meethodName` on each one  
- Config `scrawl.ext`: Automatically calls `addExtension`. See the sample extension below. cli: `scrawl -scrawl.ext group:class`  
  
### Outputs  
For pieces of information to share with other extensions  
- `addOutput($group, $key, $content)`:   
    - `# Import key "Something" not found.` gets the `$content` where `$group=='key'` and `$key=='Something'`  
    - throws if `$group, $key` is already set  
    - Any value can be set for `$content`, but `$group`s may have expectations. `$group=='key'` requires `$content` be a string  
- `setOutput($group, $key, $content)`: Same as `addOutput`, but does not throw if `$group, $key` already set  
- `getOutput($group, $key)`: Get `$content` for `$group, $key`.  
- `getOutputs($group=null)`: Get a `key=>value` array of the group specified OR of all groups if `null` is given  
  
### Existing Output Groups:  
- `$group=='key', $key='SomeThing'` is used to `# Import key "SomeThing" not found.` into markdown  
- `$group=='file', $key='Some/Path.ext'` is used to write `$content` to `PROJECT_ROOT/DOCS_DIR/Some/Path.ext`  
- @TODO programattically generate a list of all groups.  
  
  
## Sample Extensions  
Extensions are currently written in PHP. (@TODO enable extensions in other languages)  
  
### 1. Configure   
Add to your `.config/scrawl.json` (or use [another config option](/docs/Config.md)):  
```json  
{  
    "autoload": {  
        "classmap":["src"]  
    },  
    "scrawl.ext": {  
        "\\Your\\Space\\MyCoolExtension": "default"  
    }  
}  
```  
### 2. Simple Extension class  
Create `src/MyCoolExtension.php`  
```php  
<?php  
namespace Your\Space;  
class MyCoolExtension extends \Tlf\Scrawl\Extension {  
    public function onSourceFileFound($srcFile){  
        $fileList = $this->scrawl->getOutput('key', 'AllFiles');  
        $fileList .= "\n- $srcFile->relPath";  
        $this->scrawl->setOutput('key','AllFiles', $fileList);  
    }  
}  
```  
### 3. Import your new setting  
Create `docs-src/AllFiles.src.md`  
```md  
# All Source Files  
@‍import(AllFiles)  
```  
### 4. Run Scrawl  
You should have already followed the [initial guide](/README.md).  
`cd` to your project directory and execute `scrawl`. `docs/AllFiles.md` should now contain a list of all your source files.  
@TODO Move this sample extension into a test & use `# Import key "" not found.`s  
  
## Existing events/extension hooks:  
# Import key "Extensions.EventsList" not found.  
  
# Import key "Extensions.CustomCall" not found.  
