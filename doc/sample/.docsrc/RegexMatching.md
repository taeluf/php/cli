<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# RegexMatching  
You may want to read the [extension documentation](Extensions.md) if you have questions about how extensions work  
  
## Example  
Create an extension & implement `onSourceFileFound` or another function.   
### 1. Define your regex(es)  
```php  
# Import key "Regex.structure" not found.  
```  
### 2. Write your onMatch function(s)  
```php  
# Import key "Regex.onMatchFull" not found.  
```  
### 3. Call Regex Utility   
```php  
# Import key "Regex.invoke" not found.  
```  
  
## The $info array  
```php  
# Import key "Regex.infoArray" not found.  
```  
