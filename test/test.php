<?php
/**
 * To test this library execute `php test/test.php` from the root of this repo.
 * You should see 4 tests passing
 *
 */

$command = __DIR__.'/';

$tests = [
    'proto'=>'pointless output',
    'proto main'=>'pointless output',
    'proto rand -chars a -len 1'=> 'a',
    'proto evens -from 1 -to 5'=> "2\n4",
];

foreach ($tests as $c=>$t){

    ob_start();
    system("${command}${c}");
    $output = trim(ob_get_clean());
    if ($output==$t){
        echo "\n+pass: $c";
    } else {
        echo "\n-fail: $c";
        echo"\n    -$output-";
    }
}

echo "\n";
