<?php

namespace Tlf\Cli;

trait Msgs {

    /** 
     * print an error to the cli 
     *
     * @param $msg the error to highlight in red
     * @param $nobold optional message to print in default terminal color
     */
    public function error(string $msg, string $nobold=''){
        echo "\n\033[0;31m$msg\033[0m";
        if ($nobold!='')echo ': '.$nobold;
    }

    /**
     * Print a notice to the cli
     * @param $title the error to highlight in red
     * @param $message optional message to print in default terminal color
     */
    public function notice(string $title, string $message){
        echo "\n\033[0;31m$title\033[0m"
            .": ".$message;
    }
}
